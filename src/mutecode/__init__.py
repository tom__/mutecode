"""Init file of the mutecode package.
"""

import argparse

from pathlib import Path

from mutecode.exceptions import ArgsError
from mutecode.mutecode import main as mutecode


def parsing_asm_file_arg(asm_file: Path) -> Path:
    """Checking the ASM input file argument.

    Arguments:
        asm_file(Path): Input file path to check.

    Returns:
        asm_file(Path): ASM file if valid.

    Raises:
        ValueError: If the ASM input file does not exist.
    """
    asm_file = Path(asm_file)

    if asm_file.is_file():
        return asm_file

    raise ValueError(f"Input file error: {asm_file} file not found")


def parse_loader_args(args: argparse.Namespace) -> dict:
    """Checking the validity of the loader arguments.

    Arguments:
        args(argparse.Namespace): All user-defined arguments.

    Returns:
        loader_args(dict): The parsed loader arguments.

    Raises:
        ValueError: If the loader arguments are not valid.
    """
    loader_path = None

    if args.loader and Path(args.loader).is_dir():
        loader_path = Path(args.loader) / "loader"
    elif args.loader and Path(args.loader):
        loader_path = Path(args.loader)

    loader_args = {
        'loader': loader_path,
    }
    return loader_args


def parse_output_args(args: argparse.Namespace) -> dict:
    """Checking the validity of the output arguments.

    Arguments:
        args(argparse.Namespace): All user-defined arguments.

    Returns:
        output_args(dict): The output loader arguments.

    Raises:
        ValueError: If the output arguments are not valid.
    """
    output_file = None

    if args.output_file and Path(args.output_file[0]).is_file():
        raise ValueError(
            f"Output file error: {args.output_file[0]} file exist"
        )

    if isinstance(args.output_file, list):
        output_file = Path(args.output_file[0])

    output_args = {
        'silent': args.silent,
        'verbose': bool(args.verbose and not args.silent),
        'output': output_file,
    }
    return output_args


def parse_process_args(args: argparse.Namespace) -> dict:
    """Checking the validity of the process arguments.

    Arguments:
        args(argparse.Namespace): All user-defined arguments.

    Returns:
        process_args(dict): The parsed process arguments.

    Raises:
        ValueError: If the process arguments are not valid.
    """
    # Parsing of the encoding argument
    if not args.encoding:
        selected_encoding = ['ADD', 'ROTL', 'XOR']
    elif 0 < len(args.encoding) <= 3:
        selected_encoding = []
        encoding_args = [
            encoding.upper()
            for encoding in args.encoding
        ]
        if "ADD" in encoding_args:
            selected_encoding.append('ADD')
        if "ROTL" in encoding_args:
            selected_encoding.append('ROTL')
        if "XOR" in encoding_args:
            selected_encoding.append('XOR')
    elif len(args.encoding) > 3:
        raise ValueError("Encoding list is too long")
    else:
        raise ValueError(
            "An unknown error occurred during parsing of the encoding argument"
        )

    # Parsing of the metamorphism argument
    metamorphism_settings = {
        'decoder': False,
        'payload': False,
    }

    if args.metamorphism and "all" in args.metamorphism:
        metamorphism_settings['decoder'] = True
        metamorphism_settings['payload'] = True
    elif args.metamorphism and "all" "decoder" in args.metamorphism:
        metamorphism_settings['decoder'] = True
    elif args.metamorphism and "all" "payload" in args.metamorphism:
        metamorphism_settings['payload'] = True

    process_args = {
        'metamorphism': metamorphism_settings,
        'encoding': selected_encoding,
    }
    return process_args


def main():
    """Main function used to handle the mutecode arguments.

    Raises:
        ArgsError: Raised if arguments parsing fails.
    """
    # Configuration of the mutecode arguments parser
    parser = argparse.ArgumentParser(description="Performing some "
                                                 "transformations on a ASM code"
                                                 " used to create a shellcode.",
                                     add_help=False,
                                     allow_abbrev=False)

    main_arg = parser.add_argument_group("Required arguments")
    main_arg.add_argument('-i', '--input',
                          nargs=1,
                          action="store",
                          dest="source_file",
                          required=True,
                          metavar="ASM_FILE",
                          help="Path of the ASM source code file to process.")

    process_args = parser.add_argument_group("Process arguments")
    process_args.add_argument('-e', '--encoding',
                              nargs='+',
                              choices=["ADD", "ROTL", "XOR"],
                              action="store",
                              dest="encoding",
                              help="Selection of the encoding types to used. "
                                   "Default: ADD ROTL XOR")
    process_args.add_argument('-m', '--metamorphism',
                              choices=["decoder", "payload", "all"],
                              nargs=1,
                              action="store",
                              dest="metamorphism",
                              default=[],
                              help="Selection where to apply the metamorphism "
                                   "process. Default: Not used.")

    loader_args = parser.add_argument_group("Loader arguments")
    loader_args.add_argument("-l", "--loader",
                             nargs="?",
                             const="./loader",
                             help="Create a C loader with the final payload. "
                                  "Useful for quick tests. Default: "
                                  "./loader")

    output_args = parser.add_argument_group("Output arguments")
    output_args.add_argument('-o', '--output',
                             action="store",
                             nargs=1,
                             dest="output_file",
                             metavar="FILE",
                             help="Redirect the mutecode stdout into a file.")
    output_args.add_argument("-v", "--verbose",
                             action="store_true",
                             help="Include metadata about the mutecode "
                                  "process in the output.")
    output_args.add_argument("-s", "--silent",
                             action="store_true",
                             help="Display only the final payload. Inhibits "
                                  "the verbose option if both are specified.")

    help_arg = parser.add_argument_group("Help")
    help_arg.add_argument("-h", "--help",
                          action="help",
                          help="Display the mutecode help message.")

    # Construction of the dict which contains all the arguments used
    args = parser.parse_args()
    try:
        arguments = {
            'asm_file': parsing_asm_file_arg(args.source_file[0]),
        }
        arguments.update(parse_process_args(args))
        arguments.update(parse_loader_args(args))
        arguments.update(parse_output_args(args))
    except ValueError as err:
        raise ArgsError(f"{err}")

    mutecode(arguments)
