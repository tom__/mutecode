"""Main script of the mutecode project.
"""

from contextlib import redirect_stdout
from random import choice
from string import Template

import mutecode.tools.formatter as formatter
import mutecode.tools.system_interactions as sys_interact

from mutecode.exceptions import PolymorphismError
from mutecode.settings import LOADER_C_TEMPLATE
from mutecode.tools.polymorphism import PolymorphismEngine
from mutecode.tools.pretty_print import PrettyPrint
from mutecode.tools.metamorphism import MetamorphismEngine


def main(arguments: dict = None):
    """Main function of mutecode. Called from __init__ file.

    Arguments:
        arguments(dict): All the arguments used by mutecode.

    Raises:
        PolymorphismError: If the polymorphism process does not
            return the expected data structure.
    """
    # Creation of the mutecode tmp directory, if it already exists,
    # it will be removed before being recreated
    sys_interact.remove_tmp_dir(recreate=True)

    # Extracting ASM code from the input file
    with arguments['asm_file'].open('r') as payload_asm:
        payload_asm = payload_asm.read()

    # Apply metamorphism on the input payload if requested
    if arguments['metamorphism']['payload']:
        payload_asm = MetamorphismEngine().run(payload_asm)

    # Compile and extract the payload opcodes
    tmp_asm_file = sys_interact.create_tmp_file(payload_asm)
    _, payload = sys_interact.compile_asm(tmp_asm_file, extract=True)

    # Split the list of payload opcodes and apply polymorphism
    encoding_info = [
        (sublist, choice(arguments['encoding']))
        for sublist in formatter.random_split_list(payload)
    ]
    polymorphism_engine = PolymorphismEngine(
        metamorphism=bool(arguments['metamorphism']['decoder'])
    )

    polymorphism_result = polymorphism_engine.process(encoding_info)
    try:
        payload = polymorphism_result['payload']
        decoder = polymorphism_result['decoder']
        metadata = polymorphism_result['metadata']
    except KeyError as err:
        raise PolymorphismError(f"Output error: {err}")

    # Create a C loader if requested
    if arguments['loader']:
        loader_key = {
            'shellcode_var': formatter.c_array_fmt(decoder + payload)
        }
        loader_c = Template(LOADER_C_TEMPLATE).substitute(loader_key)
        loader_path = sys_interact.create_tmp_file(loader_c)

        sys_interact.compile_c(loader_path, arguments['loader'])
        metadata['loader'] = "Created"

    # Collect metamorphism information
    if arguments['metamorphism']['decoder'] \
            and arguments['metamorphism']['payload']:
        metadata['metamorphism_scope'] = "all"

    elif arguments['metamorphism']['decoder']:
        metadata['metamorphism_scope'] = "decoder"

    elif arguments['metamorphism']['payload']:
        metadata['metamorphism_scope'] = "payload"

    # Configure and display results
    output = PrettyPrint(payload=decoder + payload,
                         metadata=metadata,
                         verbose=arguments['verbose'],
                         silent=arguments['silent'])

    if arguments['output']:
        with arguments['output'].open('w+') as output_file, \
                redirect_stdout(output_file):
            output.display()
    else:
        output.display()

    # Removing the mutecode tmp directory
    sys_interact.remove_tmp_dir()
