"""All the stuff used to apply metamorphism on ASM code.
"""

import random
import re

from typing import Optional

from mutecode.exceptions import MetamorphismError
from mutecode.settings import (
    RESET_DETECT_PATTERN,
    RESET_INSTR,
    X64_REGS,
)


class MetamorphismEngine:
    """This class is used to perform the metamorphism process on given
    ASM code.
    """

    def __init__(self):
        """Initialization of the MetamorphismEngine class. Loading
        some required information from settings to apply metamorphism
        on ASM code.
        """
        self.registers_dict = X64_REGS
        self.reset_detect_pattern = RESET_DETECT_PATTERN
        self.reset_instr = RESET_INSTR

    @staticmethod
    def _extract_reg(matched_part: str) -> Optional[str]:
        """ This method let us extract the initial register than was
        used into the matched part of ASM code.

            We can validate the matched pattern at the same time.
            If None value is returned, it means that the model
            is not eligible to a substitution.

        Arguments:
            matched_part(str): The matched part of ASM code.

        Returns:
            register(str|None): Register name of than was used in the
                initial ASM matched part.
        """
        registers = re.search(r"[a-z0-9]{0,4},\ [a-z0-9]{0,4}", matched_part)
        first_reg, second_reg = registers.group().split(', ')

        return first_reg if first_reg == second_reg else None

    def _get_tmp_register(self, origin_reg: str, nbr: int) -> tuple:
        """Selecting a random tmp register that fit with the initial
        register.

            The 'origin_reg' is required for two reasons. First, to
            know the size of tmp register to return and the second to
            be sure that the tmp register is not the same as the
            initial register.

            LIMITATION: If more than 1 tmp register is requested,
            there is a risk of overlaping. @TODO: Fix it!

        Arguments:
            origin_reg(str): The register to substitute.
            nbr(int): Number of tmp register to return.

        Returns:
            (tuple): Selected tmp register.

        Raises:
            MetamorphismError: If it's not possible to get a tmp
                register.
        """
        reg = origin_reg
        selected_tmp = []

        for _ in range(nbr):
            while reg == origin_reg:
                try:
                    reg = random.choice(self.registers_dict[origin_reg])
                except KeyError as err:
                    raise MetamorphismError(
                        f"Can't find a valid tmp reg for {origin_reg} register: "
                        f"{err}"
                    )

            selected_tmp.append(reg)

        return (*selected_tmp, )

    @staticmethod
    def clean_comments(asm: str) -> str:
        """This method is used to remove all comments from ASM source.

        Arguments:
            asm(str): ASM source code where we must to remove
                comments.

        Returns:
            asm_uncomment(str): ASM source code with all comments
                removed.
        """
        asm_uncomment = ""

        for line in asm.split('\n'):
            comment_pos = line.find(";")
            line = line if comment_pos < 0 else line[:comment_pos]
            asm_uncomment += f"{line}\n"

        return asm_uncomment

    def register_reset(self, asm: str) -> str:
        """Substitution of the ASM instructions used to reset
        register.

            Detection of reset register is based on xor/sub
            instruction.

            The padding is used to keep the input and output ASM code
            synchronized, without this, part of ASM code will be
            override.

        Arguments:
            asm(str): The ASM source code file to apply substitution.

        Returns:
            asm(str): The ASM code once the substitutions have been
                made.
        """
        padding = 0

        detection_pattern = self.reset_detect_pattern
        # Fetching the matched ASM code and position in the ASM code
        matched = [
            (matched_pattern.span(), matched_pattern.group())
            for matched_pattern in re.finditer(detection_pattern,
                                               asm,
                                               re.MULTILINE)
        ]

        # Performing substitution of the matched parts
        for matched_pos, matched_str in matched:
            origin_reg = self._extract_reg(matched_str)

            if origin_reg:
                start, end = matched_pos
                tmp_reg_nbr, new_instr = random.choice(self.reset_instr)
                tmp_reg = self._get_tmp_register(origin_reg, nbr=tmp_reg_nbr)

                asm = asm[:start + padding] \
                    + new_instr.format(origin_reg, *tmp_reg) \
                    + asm[end + padding:]
                padding += len(new_instr) - len(matched_str)

        return asm

    def run(self, asm: str) -> str:
        """Main method of MetamorphismEngine. It's used to organize
        the order of metamorphism operations to apply.

        Arguments:
            asm(str): ASM source code to process.

        Returns:
            asm(str): ASM code after applying all substitution
                operations.
        """
        metamorphism_tasks = {
            '1': self.clean_comments,
            '2': self.register_reset,
        }

        for _, task in metamorphism_tasks.items():
            asm = task(asm)

        return asm

    def values_handling(self):
        """Substitution of the ASM instructions used to put value into
        a register.

            @TODO: Implement this!
        store_value = [
            "mov dst, src;\n",
            "push src; \npop dst;\n",
            "push value; \nxchg dst, [rsp];\n",  # inc rsp 8 oct
        ]
        """
        raise NotImplementedError("Value handling is not implemented yet")

    def incr_decr_value(self):
        """Substitution of the ASM instructions used to increment or
        decrement a value.

            @TODO: Implement this!
        dec/inc = [
            dec/inc, # xX
            sub counter, 1,
            add counter, 1,
        ]
        """
        raise NotImplementedError("Incr/decr is not implemented yet")
