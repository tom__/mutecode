"""All the stuff in charge to change the opcodes representation.
"""

import random
import re

from typing import (
    Iterator,
    Union,
)

from mutecode.exceptions import FormatterError


def _check_fmt(raw_data: Union[list, str], expected_fmt: str) -> bool:
    """Comparison between the current data format and the expected
    format.

        This function is only used in this file to check the format
        before formatting.

    Arguments:
        raw_data(Union[list, str]): Actual data format.
        expected_fmt(str): Expected format. ('list' or 'c_array')

    Returns:
        (bool): If the current format of the data match with expected
            format return True.
    """
    if expected_fmt == "list" and isinstance(raw_data, list):
        return True

    if expected_fmt == "c_array" \
        and isinstance(raw_data, str) \
            and re.match(r"unsigned char", raw_data.strip()):
        return True

    return False


def split_list(list_data: list, sublist_nbr: int) -> Iterator[list]:
    """Split the list of opcodes into several sublists with a maximum
    length and provided generator to iter on them.

    Arguments:
        list_data(list): List of opcodes.
        sublist_nbr(int): Max elements by sublist.

    Yield:
        Returning all sublists one by one.
    """
    for i in range(0, len(list_data), sublist_nbr):
        yield list_data[i:i + sublist_nbr]


def random_split_list(list_data: list) -> Iterator[list]:
    """Split the list of opcodes into several sublists with a pseudo
    random length and provided generator to iter on them.

    Arguments:
        list_data(list): List of opcodes.

    Yield:
        Returning all sublists one by one.
    """
    while list_data:
        end = random.randrange(10, 21)
        yield list_data[:end]
        list_data = list_data[end:]


def c_array_fmt(list_data: list) -> str:
    """Transforming the opcodes list to a C array representation.

    Arguments:
        list_data(list): List of opcodes.

    Returns:
        c_array(str): C array representation of the opcodes
            list.

    Raises:
        FormatterError: If the input data is not formatted as it
            should be.
    """
    if not _check_fmt(list_data, "list"):
        raise FormatterError("Input data is not a list")

    c_array = ""
    for sublist in split_list(list_data, 10):
        c_array += '\t' + ', '.join(sublist) + ',\n'
    c_array = f"unsigned char shellcode[] = {{\n{c_array}}};\n"

    return c_array


def list_fmt(c_array: str) -> list:
    """Transforming the opcodes C array representation to a list.

    Arguments:
        c_array(str): C array data representation.

    Returns:
        opcode_list(list): List of opcodes.

    Raises:
        FormatterError: If the input data can't to be formatted.
    """
    if not _check_fmt(c_array, "c_array"):
        raise FormatterError("Input data don't match with c_array "
                             "representation")

    re_opcode = re.compile(r"^0x[0-9A-f]{2},*$")
    opcode_list = [
        opcode.replace(',', '')
        for opcode in c_array.split()
        if re.match(re_opcode, opcode)
    ]

    return opcode_list
