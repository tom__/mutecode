"""All the stuff used to apply polymorphism on ASM code.
"""

import random
import re

from string import Template
from typing import Tuple

from mutecode.exceptions import PolymorphismError
from mutecode.settings import (
    BAD_OPCODES,
    FOOTER_DECODER_TPL,
    HEADER_DECODER_TPL,
    PART_DECODER_TPL,
    JUMP_DECODER_TPL,
)
from mutecode.tools.system_interactions import (
    compile_asm,
    create_tmp_file,
)
from mutecode.tools.metamorphism import MetamorphismEngine


class PolymorphismEngine:
    """This class is used to perform the polymorphism process on given
    ASM code.
    """

    def __init__(self, metamorphism: bool = False):
        """Initialization of the PolymorphismEngine class. Loading
        some required information from settings to apply the
        metamorphism transformations and to build decoder.

            See README for more information about the templates
            structures used in this class.

        Arguments:
            metamorphism(bool): If set, metamorphism transformations
                will be applied on the decoder ASM code.
        """
        self._i = 0

        self.blacklist_opcodes = BAD_OPCODES
        self.tpl = {
            'footer': FOOTER_DECODER_TPL,
            'header': HEADER_DECODER_TPL,
            'part': PART_DECODER_TPL,
            'jump': JUMP_DECODER_TPL,
        }

        self.polymorphism_result = {
            'payload': [],
            'decoder': [],
            'metadata': {},
        }

        self.metamorphism = metamorphism

    def bad_opcodes(self, opcodes: list) -> bool:
        """Check for bad opcodes in the given list of opcodes.

        Arguments:
            opcodes(list): List containing the opcodes to check.

        Returns:
            (bool): If True, it means that bad opcodes have been found
                in the list
        """
        bad = 0

        for bad_value in self.blacklist_opcodes:
            bad += opcodes.count(bad_value)

        return bool(bad)

    def _build_decoder_tpl(self) -> Template:
        """This method is used to build the final decoder template.

            When this method is called, 'self._i' contains the number
            of encoded parts.

            Regularly, we must add a “jump part” to avoid segmentation
            errors during the execution of the payload. This is
            necessary because we use a `jump short` instruction to
            call the payload after all parts of the decoder have
            been executed.

        Returns:
            (Template): ASM decoder template used to build the final
                decoder.

        Raises:
            KeyError: If this exception is raised, it's means that
                there is a problem with the ASM templates.
        """
        # Jump parts counter
        j = 0
        decoder_tpl = ""

        # Adding the decoder header
        tpl_header = Template(self.tpl['header'])
        header_keys = {
            'PART_SIZE_N': "$part_size_0",
            'JUMP_LABEL_N': "jump_label_0",
        }

        decoder_tpl += tpl_header.safe_substitute(header_keys)

        for i in range(self._i):
            # Get all conditions necessary to determine if a jump
            # part should be added
            jump_need = not bool(self._i <= 4)
            jump_now = not bool((i + 1) % 4 != 0)
            last = not bool((self._i - 5) < i)

            # Adding a jump part to the decoder template
            if jump_need and jump_now and not last:
                tpl_jump = Template(self.tpl['jump'])
                jmp_keys = {
                    'PART_LABEL_N': f"$part_label_{i}",
                    'JUMP_LABEL_N': f"jump_label_{j}",
                    'JUMP_LABEL_NU': f"jump_label_{j + 1}",
                }
                decoder_tpl += tpl_jump.substitute(jmp_keys)
                j += 1

            # Adding a decoder part to the decoder template
            tpl_part = Template(self.tpl['part'])
            part_keys = {
                'PART_LABEL_N': f"$part_label_{i}",
                'PART_SIZE_N': f"$part_size_{i}",
                'PART_SIZE_NU': f"$part_size_{i + 1}",
                'PART_KEY_N': f"$part_key_{i}",
                'DECODER_INSTR_N': f"$decoder_instr_{i}",
            }
            decoder_tpl += tpl_part.substitute(part_keys)

        # Adding the decoder footer
        tpl_footer = Template(self.tpl['footer'])
        footer_keys = {
            'JUMP_LABEL_N': f"jump_label_{j}",
        }
        decoder_tpl += tpl_footer.substitute(footer_keys)

        return Template(decoder_tpl)

    def _build_decoder(self, decoder_info: dict) -> list:
        """This method is in charge of the last step of building the
        ASM code of the decoder.

            Check the mutecode wiki for more information about the
            expected structure of the 'decoder_info' dict.

        Arguments:
            decoder_info(dict): All information required to decode
                payload.

        Returns:
            decoder(list): The list of decoder opcodes.

        Raises:
            PolymorphismError: Raise if we can't get the decoder
                template.
            MetamorphismError: Raised when we can't apply the
                metamorphism operations on the decoder ASM code.
        """
        # Getting the decoder template
        try:
            decoder_tpl = self._build_decoder_tpl()
        except KeyError as err:
            raise PolymorphismError(
                f"Building of decoder template failed: {err}"
            )

        # Placing all information about the encoded payload used by
        # the decoder
        decoder_asm = decoder_tpl.safe_substitute(decoder_info)

        # Removing the junk instruction produced by the decoder
        # template building method
        decoder_asm = ''.join(f'{line}\n'
                              for line in decoder_asm.split('\n')
                              if not re.search(r".*\$part_size_[1-9]+.*",
                                               line))

        # Applying metamorphism on the decoder code if required
        if self.metamorphism:
            decoder_asm = MetamorphismEngine().run(decoder_asm)

        # Compiling and extracting opcodes from the ASM decoder code
        path_decoder = create_tmp_file(decoder_asm)
        _, decoder = compile_asm(path_decoder, extract=True)

        return decoder

    def add_encoding(self, plain_opcodes: list) -> Tuple[list, int]:
        """Iterating over the opcodes list and adding a same random
        value on each of them.

        Arguments:
            plain_opcodes(list): The list containing the initial
                payload opcodes.

        Returns:
            (tuple): Contains the list of encoded opcodes and the key
                that was added on each opcode.

        Raises:
            ValueError: If a bad opcode value has been detected.
        """
        encoded = []
        key = random.randint(1, 255)

        for opcode in plain_opcodes:
            opcode = int(opcode, 16) + key
            opcode = opcode if opcode <= 255 else opcode % 255 - 1

            encoded.append(f"{opcode:#04x}")

        if self.bad_opcodes(encoded):
            raise ValueError("Encoded part contains a bad opcode value")

        return encoded, key

    def rotl_encoding(self, plain_opcodes: list) -> Tuple[list, int]:
        """Iterating over the opcodes list and perform a right shift
        on each of them.

        Arguments:
            plain_opcodes(list): The list containing the initial
                payload opcodes.

        Returns:
            (tuple): Contains the list of encoded opcodes and the key
                that was used to shift each opcode.

        Raises:
            ValueError: If a bad opcode value has been detected.
        """
        encoded = []
        key = random.randint(1, 8)

        for opcode in plain_opcodes:
            opcode = format(int(opcode, 16), '0>8b')
            _left = opcode[0:key]
            _right = opcode[key:8]
            opcode = f"{_right}{_left}"
            encoded.append(f"{int(opcode, 2):#04x}")

        if self.bad_opcodes(encoded):
            raise ValueError("Encoded part contains a bad opcode value")

        return encoded, key

    def xor_encoding(self, plain_opcodes: list) -> Tuple[list, str]:
        """Iterating over the opcodes list and perform a XOR operation
        on each of them.

        Arguments:
            plain_opcodes(list): The list containing the initial
                payload opcodes.

        Returns:
            (tuple): Contains the list of encoded opcodes and the key
                that was used to XOR each opcode.

        Raises:
            ValueError: If a bad opcode value has been detected.
        """
        encoded = []
        key = hex(random.randint(1, 255))

        for opcode in plain_opcodes:
            opcode = int(opcode, 16) ^ int(key, 16)
            encoded.append(f"{opcode:#04x}")

        if self.bad_opcodes(encoded):
            raise ValueError("Encoded part contains a bad opcode value")

        return encoded, key

    def encoding_manager(self,
                         plain_opcodes: list,
                         encoding: str) -> Tuple[list, str, int]:
        """Calling the appropriate method to encode the given opcode
        list and checking the result of this method.

            The limit of five attempts to encode the given opcode list
            is completely arbitrary, just to avoid an infinite loop.

        Arguments:
            plain_opcodes(list): Initial opcodes to encode.
            encoding(str): Type of encoding to apply on opcodes.

        Returns:
            (tuple): Contains the list of encoded opcodes, the x64
                instruction used to decode it and the key used to
                encode them.

        Raises:
            PolymorphismError: This exception can be raised for two
                reasons, firstly if the requested type of encoding is
                not implemented, secondly, if we fail five times to
                have a list of encoded opcodes without invalid opcodes
                value.
        """
        attempt = 0
        while attempt <= 5:
            try:
                if encoding == "ADD":
                    encoded, key = self.add_encoding(plain_opcodes)
                    decoder_instr = "sub"
                elif encoding == "ROTL":
                    encoded, key = self.rotl_encoding(plain_opcodes)
                    decoder_instr = "ror"
                elif encoding == "XOR":
                    encoded, key = self.xor_encoding(plain_opcodes)
                    decoder_instr = "xor"
                else:
                    raise PolymorphismError("Unknown encoding type")

                return encoded, decoder_instr, key

            except ValueError:
                attempt += 1
                if attempt > 5:
                    raise PolymorphismError(
                        "Fail to encode the opcodes list without bad value"
                    )

    def process(self, encoding_info: list) -> dict:
        """Main method of the polymorphism engine. It's used to encode
        the payload opcodes and to build the appropriate decoder.

            See README for more information about the 'encoding_info'
            and the 'self.polymorphism_result' dict structure.

        Arguments:
            encoding_info(dict): Contains the list of payload opcodes
                divided into sublists and the type of encoding to
                apply for each sublist.

        Returns:
            self.polymorphism_result(dict): Contains the list of
                encoded payload opcodes, the list of decoder opcodes
                and a dict which contains all the metadata on the
                polymorphism process.
        """
        decoders_info = {}

        for plain_opcodes, encoding in encoding_info:
            # Encoding of the payload part and getting the encoding
            # process information
            encoded, decoder_instr, key = self.encoding_manager(plain_opcodes,
                                                                encoding)

            # Adding require information about the current part to
            # build decoder
            decoders_info.update(
                {
                    f'decoder_instr_{self._i}': decoder_instr,
                    f'part_label_{self._i}': f"label_{self._i}",
                    f'part_size_{self._i}': len(encoded),
                    f'part_key_{self._i}': key,
                }
            )

            self.polymorphism_result['payload'] += encoded
            self.polymorphism_result['metadata'][f'part_{self._i}'] = {
                'size': len(encoded),
                'encoding': encoding,
                'key': key,
            }
            self._i += 1

        self.polymorphism_result['decoder'] = self._build_decoder(decoders_info)

        total_length = len(self.polymorphism_result['decoder']
                           + self.polymorphism_result['payload'])
        self.polymorphism_result['metadata']['total_parts'] = self._i
        self.polymorphism_result['metadata']['total_length'] = total_length

        return self.polymorphism_result
