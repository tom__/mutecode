# pylint: disable=anomalous-backslash-in-string
"""All the stuff in charge to display the mutecode processing
information.
"""

from contextlib import suppress

from mutecode.tools.formatter import c_array_fmt


MUTECODE_HEADER = """
\t              _         ___          _      
\t  /\/\  _   _| |_ ___  / __\___   __| | ___ 
\t /    \| | | | __/ _ \/ /  / _ \ / _` |/ _ \\
\t/ /\/\ \ |_| | ||  __/ /__| (_) | (_| |  __/
\t\/    \/\__,_|\__\___\____/\___/ \__,_|\___|
"""


class PrettyPrint:
    """This class manages how to display information about the
    mutecode processing.
    """

    def __init__(self,
                 payload: list,
                 metadata: dict,
                 silent: bool,
                 verbose: bool):
        """Initialization of PrettyPrint class with all required
        information.

            Check the mutecode wiki for more information about the
            expected structure of the 'metadata' dict.

        Arguments:
            payload(list): List containing all payload opcodes.
            metadata(dict): This dict containing information about
                payload as encoding, size and so on.
            silent(bool): If set, only the payload will be displayed.
            verbose(bool): If set, all metadata plus the mutecode
                header will be displayed.
        """
        self.payload = payload
        self.metadata = metadata
        self.silent = silent
        self.verbose = verbose

    @staticmethod
    def display_verbose(metadata: dict):
        """This method display the metadata information about mutecode
        processing.

        Arguments:
            metadata(dict): All metadata to display. See wiki for more
                information.
        """
        # General information whitelist
        general_info = [
            "total_parts", "total_length",
            "metamorphism_scope", "loader",
        ]
        border = ' ' + '-' * 43

        print("\nMetadata:")
        # Displaying the general information
        print(border)
        for general_key in general_info:
            with suppress(KeyError):
                general_part = f"{general_key}: {metadata[f'{general_key}']}"
                padding_general = ' ' * (40 - len(f'{general_part}'))
                print(f" | {general_part}{padding_general}|")

        # Displaying the encoded parts information
        print(border)
        for i in range(metadata['total_parts']):
            padding_title = f"{' ' * (20 - (6 + len(f'{i + 1}')))}"
            title_part = f" Part {i + 1}{padding_title}"

            for title, info in metadata[f'part_{i}'].items():
                info_part = f" {title}: {info}"
                padding = ' ' * (20 - len(info_part))
                print(f" |{title_part}|{info_part}{padding}|")
                title_part = ' ' * 20

            print(f"{border}")

    def display(self):
        """This is the main method of PrettyPrint class.

            As you can see the silent inhibits the verbose option,
            so if the both are set, only payload will be displayed.
        """
        if not self.silent:
            print(MUTECODE_HEADER)

            if self.verbose:
                self.display_verbose(self.metadata)
            print("\nPayload:")

        print(f"{c_array_fmt(self.payload)}")
