"""All the stuff used to interact with system.
"""

import shutil

from datetime import datetime
from pathlib import Path
from typing import (
    Optional,
    Tuple,
)
from plumbum import local

from mutecode.settings import TMP_PATH_DIR
from mutecode.tools.formatter import list_fmt


def remove_tmp_dir(recreate: bool = False):
    """Removing the mutecode tmp directory.

    Arguments:
        recreate(bool): If set, the mutecode tmp directory will be
            recreated.
    """
    if TMP_PATH_DIR.exists() and TMP_PATH_DIR.is_dir():
        shutil.rmtree(TMP_PATH_DIR)

    if recreate:
        TMP_PATH_DIR.mkdir()


def create_tmp_file(content: str) -> Path:
    """Create tmp file

    Arguments:
        content(str): Content to write in the tmp file.

    Returns:
        file_path(Path): Path of the tmp file.
    """
    file_path = TMP_PATH_DIR / f"tmp_{datetime.now().timestamp()}"

    with file_path.open("w+") as file:
        file.write(content)

    return file_path


def compile_asm(src_path: Path,
                extract: bool = False) -> Tuple[Path, Optional[list]]:
    """Compiling the given ASM x64 source file.

    Arguments:
        src_path(Path): Path of the ASM source file.
        extract(bool): If set, the opcodes will be extracted from
            binary file.

    Returns:
        (tuple): Contains the path of the ASM compiled binary file and
            if 'extract' argument is set, the opcodes list.

    Raises:
        ProcessExecutionError: If 'nasm' command returns a non-zero
            exit code.
    """
    opcodes = None

    bin_path = TMP_PATH_DIR / src_path.with_suffix(".bin").name
    local["nasm"]("-f", "bin", src_path, "-o", bin_path)

    if extract:
        opcodes = local["xxd"]("-i", bin_path)
        opcodes = list_fmt(opcodes)

    return bin_path, opcodes


def compile_c(src_path: Path, bin_path: Path) -> Path:
    """Compiling the given C source file.

    Arguments:
        src_path(Path): Path of the ASM source file.
        bin_path(Path): Path of the C compiled file.

    Returns:
        bin_path(Path): Path of the C compiled file.

    Raises:
        ProcessExecutionError: If 'gcc' command returns a non-zero
            exit code.
    """
    local['gcc']('-x', 'c', src_path, '-o', bin_path)
    return bin_path
