"""Declaration of the mutecode exceptions.
"""

RED = '\033[91m'
NC = '\033[0m'


class MutecodeError(Exception):
    """Base class of the mutecode exceptions.
    """

    def __init__(self, msg: str = None):
        exception_msg = "Mutecode error has occurred"
        if msg:
            exception_msg = msg

        super().__init__(exception_msg)


class ArgsError(MutecodeError):
    """Exception related to arguments parsing.
    """

    def __init__(self, msg: str = None):
        exception_msg = "Arguments error"
        if msg:
            exception_msg += f"\n{RED}{msg}{NC}"

        super().__init__(exception_msg)


class FormatterError(MutecodeError):
    """Exception related to any formatter operations.
    """

    def __init__(self, msg: str = None):
        exception_msg = "Formatter error has occurred"
        if msg:
            exception_msg += f"\n{RED}{msg}{NC}"

        super().__init__(exception_msg)


class PolymorphismError(MutecodeError):
    """Exception related to any polymorphism operations.
    """

    def __init__(self, msg: str = None):
        exception_msg = "Polymorphism error has occurred"
        if msg:
            exception_msg += f"\n{RED}{msg}{NC}"

        super().__init__(exception_msg)


class MetamorphismError(MutecodeError):
    """Exception related to any metamorphism operations.
    """

    def __init__(self, msg: str = None):
        exception_msg = "Metamorphism error has occurred"
        if msg:
            exception_msg += f"\n{RED}{msg}{NC}"

        super().__init__(exception_msg)
