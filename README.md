
Mutecode is a simple CLI tool written in Python. This tool allows you to easily manipulate your shellcode
from the ASM source code. You can perform certain operations on your payload such as encoding or instruction
substitutions. Currently, only the **x64 architecture** is supported.


[[_TOC_]]


## Key features
* Change your payload signature by:
  * Polymorphism (encode with ADD, XOR and/or ROTL)
  * Metamorphism
* Create a loader to quickly test and debug a payload 
* Can be used in others projects


## Installation
**1. Installing of the requirements**
```bash
apt-get install -y git python3 gcc xxd 
```

**2. Downloading and installing of mutecode**
```bash
git clone git@gitlab.com:tom__/mutecode.git
cd ./mutecode
pip3 install ./
```
... Or in a more direct way
```bash
wget -O - https://gitlab.com/tom__/mutecode/-/raw/master/install.sh | bash
```

**Compatibility:** Python 3.6 or higher


## Usage

```bash
usage: mutecode -i ASM_FILE [-e {ADD,ROTL,XOR} [{ADD,ROTL,XOR} ...]]
                [-m {decoder,payload,all}] [-l [LOADER]] [-o FILE] [-v] [-s]
                [-h]

Performing some transformations on a ASM code used to create a shellcode.

Required arguments:
  -i ASM_FILE, --input ASM_FILE
                        Path of the ASM source code file to process.

Process arguments:
  -e {ADD,ROTL,XOR} [{ADD,ROTL,XOR} ...], --encoding {ADD,ROTL,XOR} [{ADD,ROTL,XOR} ...]
                        Selection of the encoding types to use.
                        Default: ADD ROTL XOR
  -m {decoder,payload,all}, --metamorphism {decoder,payload,all}
                        Selection where to apply the metamorphism process.
                        Default: Not used.

Loader arguments:
  -l [LOADER], --loader [LOADER]
                        Create a C loader with the final payload. Useful for
                        quick tests. Default: ./loader

Output arguments:
  -o FILE, --output FILE
                        Redirect the mutecode stdout into a file.
  -v, --verbose         Include metadata about the mutecode process in the
                        output.
  -s, --silent          Display only the final payload. Inhibits the verbose
                        option if both are specified.

Help:
  -h, --help            Display the mutecode help message.

```

**Polymorphism implementation in Mutecode:**

Mutecode can encode your payload with three different methods, ADD, XOR and ROTL (Rotate left with carry).
The key to each encoding process is pseudo-random and there is a check to avoid generation of null-bytes.

By default, all methods will be used but you can specify the encoding to use if you wants.

**Metamorphism implementation in Mutecode:**

Today, the metamorphism process implemented in Mutecode is quite basic. We only substitute "reset instructions"
in other words, the instructions used to reset a register such as `xor [REG], [REG]` and `sub [REG], [REG]`. 

See below the list of possible replacement instructions:
```
xor [REG], [REG];
sub [REG], [REG];
shr [REG], 63; shr [REG], 1;
shl [REG], 63; shl [REG], 1;
mov [REGTMP], [REG]; not [REGTMP]; and [REG], [REGTMP]
mov [REGTMP], [REG]; not [REGTMP]; add [REG], [REGTMP]; inc [REG];
mov [REGTMP], [REG]; not [REGTMP]; or [REG], [REGTMP]; not [REG];
```

This process can be used on decoders, payload, or both. By default, Mutecode does not apply the metamorphism 
transformations. You can control this behavior with `--metamorphism` option.


**Output information:**

There are several options to control the output of Mutecode such as `--silent`,` --verbose` or `--output` but 
for the moment, the final payload has still the same form, a C array. In a later version, other formats will be 
implemented.

Mutecode output with the `--silent` option:

```bash
> mutecode -i asm_examples/ls.asm -s
unsigned char shellcode[] = {
    0xeb, 0x33, 0x5e, 0x48, 0x31, 0xc9, 0xb1, 0x12, 0x80, 0x6c,
    0x0e, 0xff, 0x57, 0xfe, 0xc9, 0x75, 0xf7, 0xb1, 0x0f, 0x48,
    0x83, 0xc6, 0x12, 0x80, 0x6c, 0x0e, 0xff, 0xbd, 0xfe, 0xc9,
    0x75, 0xf7, 0xb1, 0x03, 0x48, 0x83, 0xc6, 0x0f, 0x80, 0x74,
    0x0e, 0xff, 0xd6, 0xfe, 0xc9, 0x75, 0xf7, 0x48, 0x83, 0xc6,
    0x03, 0xeb, 0x05, 0xe8, 0xc8, 0xff, 0xff, 0xff, 0x9f, 0x88,
    0x32, 0x9f, 0x12, 0x86, 0x86, 0xb9, 0xc0, 0xc5, 0x86, 0xc3,
    0xca, 0x9f, 0x18, 0x42, 0x5f, 0xaa, 0x05, 0x46, 0xa4, 0x05,
    0xee, 0x7d, 0x0d, 0x05, 0x46, 0x9f, 0x14, 0x05, 0x46, 0xa3,
    0x6d, 0xed, 0xd9, 0xd3,
};
```


## Mutecode workflow

The basic Mutecode workflow:

1. Metamophism on the payload (optional)
2. Compilation of the ASM payload code
3. Division of opcodes into a pseudo-random number of parts with a variable size
3. Encode each part with a pseudo-random key
4. Build decoders
5. Metamorphism on the decoders (optional)
6. Extracting the decoders opcodes
7. Display the final payload (decoders + payload)


## Developers 

This part intend to provide useful information on the Mutecode code. (in addition of docstrings)

### PolymorphismEngine

Source: [mutecode.tools.polymorphism](https://gitlab.com/tom__/mutecode/-/blob/master/src/mutecode/tools/polymorphism.py)

**Workflow:**

The polymorphism process is triggered by the [PolymorphismEngine.process()](https://gitlab.com/tom__/mutecode/-/blob/master/src/mutecode/tools/polymorphism.py#L332) 
method. This method takes an argument which is a list of tuples that contains the payload opcodes divided into sublists. 
Each sublist is linked to a type of encoder(ADD, XOR, ROTL) and represent part of the payload. See below for an example 
of a valid argument.

```python
polymorph_arg = [
    (["0x00", "0x01", "0x02"], 'ADD'),
    (["0x03", "0x04", "0x05"], 'XOR'),
    (["0x06", "0x07", "0x08"], 'ROTL'),
]
```

There are two important phases in this method which are the encoding of each payload part and the construction of
the decoders ASM code to decode your payload when it is loaded into memory.

For each part, the [PolymorphismEngine.encoding_manager()](https://gitlab.com/tom__/mutecode/-/blob/master/src/mutecode/tools/polymorphism.py#L283) 
method is called to try to encode the opcodes. This method includes a fallback to control the encoding process. 
After each encoding process, we check that no bad opcodes are produced, if this is the case, we will choose a new encoding 
key and we try again the encoding process. The encoding process can fail five times, beyond this limit the Mutecode 
process raises an exception and exits. This behavior avoids a possible infinite loop.

You can control the list of bad opcode if you override the `self.blacklist_opcodes` attribute. This is a list, 
see [settings.py](https://gitlab.com/tom__/mutecode/-/blob/master/src/mutecode/settings.py). 

When all parts of your payload are encoded, the ASM code for the decoders will be built and compiled.
This process is managed by the [PolymorphismEngine._build_decoder()](https://gitlab.com/tom__/mutecode/-/blob/master/src/mutecode/tools/polymorphism.py#L148)
method. This method will try to build a decoder template to place the values​obtained during the encoding phase.
(see the structures below).

To build the decoder template we use ASM code parts declared in `settings`. At the level of the class, you can modify 
these templates if you override the `self.tpl` attribute. Jump part is a trick we use to avoid segfault when 
we call the payload after all the parts are decoded.  

```
self.tpl = {
    'footer': Template object that represents the bottom of ASM file,
    'header': Template object that represents the top of ASM file,
    'part': Template object that used to decode one part,
    'jump': Template object used to add a jump label,
}
```

Finally, to build the decoder model, we need to use an intermediate substitution, below you can see all the symbols
that we use by ASM template part:

- `i`: Number of the actual decoder part
- `j`: Number of the actual jump part
- `N`: Same as `i` but in the primary ASM template
- `NU`: Next upper part, in other word `N + 1` 
```
header_keys = {
    'PART_SIZE_N': "$part_size_0",
    'JUMP_LABEL_N': "jump_label_0",
}

footer_keys = {
    'JUMP_LABEL_N': f"jump_label_{j}",
}

part_keys = {
    'PART_LABEL_N': f"$part_label_{i}",
    'PART_SIZE_N': f"$part_size_{i}",
    'PART_SIZE_NU': f"$part_size_{i + 1}",
    'PART_KEY_N': f"$part_key_{i}",
    'DECODER_INSTR_N': f"$decoder_instr_{i}",
}

jmp_keys = {
    'PART_LABEL_N': f"$part_label_{i}",
    'JUMP_LABEL_N': f"jump_label_{j}",
    'JUMP_LABEL_NU': f"jump_label_{j + 1}",
}
```

All the values of these python dict start with a `$` will be replaced when the information of the decoders is 
placed to build the final decoder code. 


**Data structures:**

There are two important structures you should know if you want to use the `PolymorphismEngine`.

Firstly, the `self.polymorphism_result` dict, it will be returned if all the operations of the `PolymorphismEngine` 
are successfully performed:

```
self.polymorphism_result = {
    'payload': List of the payload opcodes (after encoding process),
    'decoder': List of the decoders opcodes,
    'metadata': {
        'total_length': Total size of the final payload(decoders + payload),
        'total_parts': Number of the opcodes parts,
        'part_n': {
            'size': Size of the part n,
            'encoding': Encoding chosen for this part,
            'key': Key used to encode this part,
        },
    },
}
```

The second structure is used to build the final ASM code of the decoder. These values below will be placed in the
final decoder template.

```
decoders_info = {
    'decoder_instr_n': Instruction to use to decode part n,
    'part_label_n': ASM label used for part n,
    'part_size_n': Size of part n,
    'part_key_n': Key to use to decode part n,
}
```
