from setuptools import (
    find_packages,
    setup,
)

install_deps = [
    "plumbum==1.6.8",
]

test_deps = [
    "mock",
    "pytest",
    "pytest-cov",
    "pytest-mccabe",
    "pytest-pylint",
]

setup_deps = [
    "pytest-runner",
]

setup(
    name="mutecode",
    version="1.0.0",
    author='Tommy Boiret',
    author_email='boiret.t@gmail.com',
    description="Help you to perform some transformations on your ASM codes",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/tom__/mutecode",
    package_dir={
        '': 'src',
    },
    packages=find_packages('src'),
    license="GPL3",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: GPL3 License",
        "Operating System :: UNIX",
    ],
    python_requires=">=3.6",
    install_requires=install_deps,
    tests_require=test_deps,
    setup_requires=setup_deps,
    extras_require={
        "test": test_deps,
    },
    entry_points={
        "console_scripts": [
            "mutecode = mutecode:main",
        ],
    },
)
