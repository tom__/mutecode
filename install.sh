#!/usr/bin/env bash
set -o nounset -o errexit -o pipefail

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'


diff(){
    #-- Take two array and return the difference
    #--               - Thank StackOverflow
    awk 'BEGIN{RS=ORS=" "}
         {NR==FNR?a[$0]++:a[$0]--}
         END{for(k in a)if(a[k])print k}' <(echo -n "${!1}") <(echo -n "${!2}")
}

msg_err(){
    #-- Display error message and exit
    local msg="${1-}"

    echo -e "${RED}\n${msg}${NC}"
    exit 1
}

msg_ok(){
    #-- Display message about the install execution
    local msg="${1-}"

    echo -e "${GREEN}\n${msg}${NC}"
}


# Checking all requirement
msg_ok "Step 1: Checking system requirements"

sys_dep=("gcc" "git" "nasm" "python" "wget" "xxd")
for dep_sys in ${sys_dep[@]}; do
    which "${dep_sys}" &> "/dev/null" && err_sys=0 || err_sys=1
    if [[ "${err_sys}" == 1 ]]; then
        msg_err "Package ${dep_sys} not found, please read requirements"
    else
        echo -e "Package ${dep_sys}: OK"
    fi
done

# Finding a compatible python interpreter
msg_ok "Step 2: Checking the python environment requirements"

if [[ $(python --version &> "/dev/null"| cut -f 2 -d ' ' | cut -f 2 -d '.') -ge 6 ]]; then
    valid_python="python"
elif [[ $(python3 --version | cut -f 2 -d ' ' | cut -f 2 -d '.') -ge 6 ]]; then
    valid_python="python3"
else
    msg_err "No valid Python interpreter was found, this project is compatible with the python 3.6 or higher versions"
fi
echo -e "Valid Python interpreter was found: ${valid_python}"

# Checking pip
"${valid_python}" -m pip -h &> "/dev/null" && echo -e "pip module was found" ||
    msg_err "pip was not found, you need to install it"

# Downloading project and installing the python package
msg_ok "Step 3: Downloading mutecode project"
if [[ -d "./mutecode" ]]; then
    msg_err "A directory ./mutecode was found. Aborting installation"
fi
git clone --progress git@gitlab.com:tom__/mutecode.git
cd "./mutecode"

msg_ok "Step 4: Installing mutecode module"

${valid_python} -m pip install ./ && err_pip="${?}" || err_pip=1
if [[ "${err_pip}" == 1 ]]; then
    msg_err "Installation of mutecode module failed"
fi

# Removing the mutecode project folders
msg_ok "Step 5: Cleaning installation files"

cd "../"
rm -rf "./mutecode"
msg_ok "You can now use the mutecode command, enjoy!"
