"""All tests for code of mutecode.tools.system_interactions
"""

import re

from mutecode.tools.metamorphism import MetamorphismEngine


FAKE_ASM_CODE = """
BITS 64
_start:
    ;COMMENT_TO_REMOVE
    xor rbx, rbx ; COMMENT_TO_REMOVE
    xor rbx, rax
    xor ax, ax
xor rbx, rbx
    xor r15, r15 ; COMMENT_TO_REMOVE
sub rcx, rcx
;COMMENT_TO_REMOVE
    mov 
    syscall
"""


def test_metamorphism():
    """Test the behavior of the Metamorphism class in charge to apply
    some substitutions on ASM code.
    """

    asm_meta = MetamorphismEngine().run(asm=FAKE_ASM_CODE)

    assert asm_meta.find("COMMENT_TO_REMOVE") == -1
    assert re.search(r"xor rbx, rax", asm_meta)
