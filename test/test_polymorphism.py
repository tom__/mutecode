"""All tests for code of mutecode.tools.system_interactions
"""

import pathlib
import pytest
from unittest.mock import patch

from mutecode.tools.polymorphism import (
    PolymorphismError,
    PolymorphismEngine,
)


OPCODE1 = [
    "0x01", "0x01", "0x01", "0x01", "0x01",
    "0x01", "0x01", "0x01", "0x01", "0x01",
    "0x01", "0x01", "0x01", "0x01", "0x01",
    "0x01", "0x01", "0x01", "0x01", "0x01",
]
OPCODE2 = [
    "0x02", "0x02", "0x02", "0x02", "0x02",
    "0x02", "0x02", "0x02", "0x02", "0x02",
    "0x02", "0x02", "0x02", "0x02", "0x02",
    "0x02", "0x02", "0x02", "0x02", "0x02",
]
OPCODE3 = [
    "0x03", "0x03", "0x03", "0x03", "0x03",
    "0x03", "0x03", "0x03", "0x03", "0x03",
    "0x03", "0x03", "0x03", "0x03", "0x03",
    "0x03", "0x03", "0x03", "0x03", "0x03",
]

FAKE_ENCODING_INFO = [
    (OPCODE1, "ADD"),
    (OPCODE2, "XOR"),
    (OPCODE2, "XOR"),
    (OPCODE2, "XOR"),
    (OPCODE3, "ROTL"),
]


def test_polymorphism_ok(monkeypatch, tmp_path):
    """Test the behavior of the Polymorphism class in charge to apply
    some encoding on ASM code.
    """
    fake_tmp = pathlib.Path(tmp_path / '.mutecode')
    fake_tmp.mkdir()

    monkeypatch.setattr('mutecode.tools.system_interactions.TMP_PATH_DIR', fake_tmp)

    polymorphism_engine = PolymorphismEngine(
        metamorphism=False)

    polymorphism_result = polymorphism_engine.process(FAKE_ENCODING_INFO)

    assert len(polymorphism_result['payload']) == len(OPCODE3) * len(FAKE_ENCODING_INFO)
    assert polymorphism_result['decoder']
    assert polymorphism_result['metadata']['total_parts'] == len(FAKE_ENCODING_INFO)

    # Assertion on encoding process
    add_value = int(OPCODE1[0], 16) + int(polymorphism_result['metadata']['part_0']['key'])
    add_value = add_value if add_value <= 255 else add_value % 255 - 1
    assert polymorphism_result['payload'][0] == f"{add_value:#04x}"

    xor_value = int(OPCODE2[0], 16) ^ int(polymorphism_result['metadata']['part_1']['key'], 16)
    assert polymorphism_result['payload'][20] == f"{xor_value:#04x}"

    rotl_value = format(int(OPCODE3[0], 16), '0>8b')
    rotl_key = int(polymorphism_result['metadata']['part_4']['key'])
    rotl_value = f"{rotl_value[rotl_key:8]}{rotl_value[0:rotl_key]}"
    assert polymorphism_result['payload'][-1] == f"{int(rotl_value, 2):#04x}"

    # Check that the metamorphism will be called if necessary
    monkeypatch.setattr(
        'mutecode.tools.polymorphism.create_tmp_file',
        lambda x: pathlib.Path("foo"),
    )
    monkeypatch.setattr(
        'mutecode.tools.polymorphism.compile_asm',
        lambda *args, **kwargs: ("foo", [1, 2, 3]),
    )

    polymorphism_engine = PolymorphismEngine(
        metamorphism=True)

    with patch('mutecode.tools.polymorphism.MetamorphismEngine') as meta_patch:
        polymorphism_engine.process(FAKE_ENCODING_INFO)
        meta_patch.assert_called_once()


def test_polymorphism_raise(monkeypatch, tmp_path):
    """Check that the Polymorphism class throws an exception if
    necessary.
    """
    fake_tmp = pathlib.Path(tmp_path / '.mutecode')
    fake_tmp.mkdir()

    monkeypatch.setattr(
        'mutecode.tools.system_interactions.TMP_PATH_DIR',
        fake_tmp,
    )

    polymorphism_engine = PolymorphismEngine(
        metamorphism=False)

    # Must raise if the encoding type is not supported
    with pytest.raises(PolymorphismError):
        polymorphism_engine.process([(OPCODE1, "UKN")])

    # Must raise if an error occurs during the construction of the
    # decoder
    monkeypatch.setattr(
        'mutecode.tools.polymorphism.PolymorphismEngine._build_decoder_tpl',
        lambda *args: exec('raise KeyError'),
    )

    with pytest.raises(PolymorphismError):
        polymorphism_engine.process([(OPCODE1, "XOR")])

    # Must raise if the encoding process fails 5 times
    monkeypatch.setattr(
        'mutecode.tools.polymorphism.PolymorphismEngine.xor_encoding',
        lambda *args: exec('raise ValueError'),
    )

    with pytest.raises(PolymorphismError):
        polymorphism_engine.process([(OPCODE1, "XOR")])
