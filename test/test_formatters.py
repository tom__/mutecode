"""All tests for code of mutecode.tools.formatters
"""

import pytest

from mutecode.exceptions import FormatterError
from mutecode.tools.formatter import (
    c_array_fmt,
    list_fmt,
    random_split_list,
    split_list,
)


FAKE_C_ARRAY = """unsigned char shellcode[] = {
\t0x00, 0x42, 0xa4, 0xff, 0x9f, 0x99,
};
"""
FAKE_OPCODES_LIST = [
    "0x00", "0x42", "0xa4",
    "0xff", "0x9f", "0x99",
]
FOO_LIST = [i for i in range(1, 30)]


def test_split_list_generators():
    """Test the behavior of the two generators used to divide a list
    into several sublists.
    """
    # Sublists generator with fixed max length
    for sublist in split_list(FOO_LIST, sublist_nbr=2):
        assert len(sublist) <= 2

    # Sublists generator with a pseudo random length
    length_sublists = []
    for rng_sublist in random_split_list(FOO_LIST):
        length_sublists.append(len(rng_sublist))

    assert length_sublists.count(length_sublists[0]) != len(length_sublists)


def test_fmts():
    """Test the behavior of the functions in charge of changing the
    format of the opcodes.
    """
    assert list_fmt(FAKE_C_ARRAY) == FAKE_OPCODES_LIST

    assert c_array_fmt(FAKE_OPCODES_LIST) == FAKE_C_ARRAY

    # Must raise this exception if input format can't to be proceed
    with pytest.raises(FormatterError):
        list_fmt(FAKE_OPCODES_LIST)

    with pytest.raises(FormatterError):
        c_array_fmt(FAKE_C_ARRAY)
