"""All tests for code of mutecode.tools.system_interactions
"""

import pathlib

from mutecode.tools.system_interactions import (
    create_tmp_file,
    remove_tmp_dir,
)


def test_tmp_dir_interactions(monkeypatch, tmp_path):
    """Test the behavior of functions in charge of interactions with
    the temporary mutecode directory.
    """
    fake_tmp = pathlib.Path(tmp_path / '.mutecode')
    monkeypatch.setattr('mutecode.tools.system_interactions.TMP_PATH_DIR', fake_tmp)

    remove_tmp_dir(recreate=True)
    assert fake_tmp.is_dir()

    dummy_file = create_tmp_file(content="Dummy content")
    assert dummy_file.read_text() == "Dummy content"

    remove_tmp_dir()
    assert not fake_tmp.is_dir()
